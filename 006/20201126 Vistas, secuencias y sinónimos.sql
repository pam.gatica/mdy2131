--VISTAS
CREATE OR REPLACE VIEW v_emp_depto_80 AS
SELECT employee_id id, last_name apellido, salary sueldo, salary*12 sueldo_anual
FROM employees
WHERE department_id = 80;

SELECT *
FROM v_emp_depto_80
WHERE sueldo >= 10000
ORDER BY apellido;

DROP VIEW v_emp_depto_80;


--SECUENCIAS
CREATE SEQUENCE seq_test;

SELECT seq_test.nextval FROM dual;
CREATE TABLE demo_secuencia(
numero NUMBER);

INSERT INTO demo_secuencia (numero)
VALUES (seq_test.nextval);

CREATE SEQUENCE seq_test2
INCREMENT BY 5
START WITH 0
MINVALUE 0
MAXVALUE 1000;

INSERT INTO demo_secuencia (numero)
VALUES (seq_test2.nextval);

SELECT 'VALOR INSERTADO ' || seq_test2.currval FROM dual;

--SINONIMOS

CREATE OR REPLACE SYNONYM syn_deptos
FOR departments;

SELECT * FROM departments;
SELECT * FROM syn_deptos;

DROP SYNONYM syn_deptos;