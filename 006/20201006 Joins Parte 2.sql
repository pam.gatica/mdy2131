SELECT
    e.employee_id "ID EMPLEADO",
    e. first_name || ' ' ||  e.last_name "NOMBRE EMPLEADO",
    e.manager_id "ID JEFE",
    jefe.first_name || ' ' ||  jefe.last_name "NOMBRE JEFE"
FROM employees e
JOIN employees jefe ON e.manager_id= jefe.employee_id
ORDER BY e.employee_id;

SELECT
    m.first_name || ' ' || m.last_name "NOMBRE JEFE",
    COUNT(e.employee_id) "EMPLEADOS A CARGO"
FROM employees e
JOIN employees m ON (e.manager_id = m.employee_id)
GROUP BY m.first_name, m.last_name
ORDER BY m.last_name;


SELECT
    e.employee_id,
    NVL(e.first_name,'SIN EMPLEADOS'),
    e.last_name,
    department_id,
    NVL(d.department_name, 'SIN DEPARTAMENTO')
FROM employees e
FULL JOIN departments d USING(department_id)
ORDER BY e.employee_id;


SELECT
    e.employee_id,
    d.department_name
FROM employees e
CROSS JOIN departments d
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    d.department_name,
    l.city
FROM employees e
JOIN departments d USING(department_id)
JOIN locations l USING(location_id)
ORDER BY e.employee_id;