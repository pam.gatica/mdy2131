SELECT
    e.employee_id,
    e.first_name,
    e.last_name,
    d.department_name
FROM employees e
JOIN departments d ON e.department_id = d.department_id;

SELECT
    e.employee_id,
    e.job_id,
    j.job_title
FROM employees e 
JOIN jobs j ON e.job_id = j.job_id;

SELECT
    employee_id,
    job_id,
    job_title
FROM employees 
NATURAL JOIN jobs;

SELECT
    department_name,
    city
FROM departments
NATURAL JOIN locations;

SELECT
    last_name,
    department_name
FROM employees
JOIN departments
USING(department_id);

SELECT
    l.city,
    c.country_name
FROM locations l
JOIN countries c USING(country_id);

SELECT C.COUNTRY_NAME,
       R.REGION_NAME
FROM COUNTRIES C 
JOIN REGIONS R
USING (REGION_ID);


SELECT 
    e.first_name,
    e.last_name,
    e.job_id,
    d.department_name
FROM employees e
JOIN departments d USING(department_id)
WHERE d.department_name = 'Finance';

SELECT
    department_id,
    department_name,
    city,
    COUNT(employee_id)
FROM employees
INNER JOIN departments USING(department_id)
INNER JOIN locations USING(location_id)
GROUP BY department_id, department_name, city
HAVING COUNT(employee_id) > 20
ORDER BY department_id;