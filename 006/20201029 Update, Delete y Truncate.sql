UPDATE employees
SET department_id = 110
WHERE employee_id = 103;

UPDATE employees
SET department_id = 110
WHERE employee_id = 104;

ROLLBACK;

UPDATE employees
SET department_id = 110;

ROLLBACK;

UPDATE employees
SET department_id = 110
WHERE employee_id = 103 OR employee_id = 104;

ROLLBACK;

UPDATE employees
SET department_id = 110
WHERE employee_id IN (103,104);

UPDATE employees
SET salary = 
        CASE
            WHEN EXTRACT(YEAR FROM hire_date) = 2004 THEN ROUND(salary*1.15)
            WHEN EXTRACT(YEAR FROM hire_date) = 2005 THEN ROUND(salary*1.13)
            ELSE ROUND(salary*1.105)
        END;

ROLLBACK;

SELECT ROUND(AVG(salary)) FROM employees;
SELECT MIN(salary) FROM employees;

UPDATE employees
SET     salary = (SELECT ROUND(AVG(salary)) FROM employees)
WHERE   salary = (SELECT MIN(salary) FROM employees);

DELETE FROM employees;

DELETE FROM departments WHERE department_name = 'Capacitación';

TRUNCATE TABLE departments;