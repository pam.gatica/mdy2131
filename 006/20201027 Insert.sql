SELECT * FROM employees;

DELETE FROM employees
WHERE department_id IS NULL;

SAVEPOINT A;

UPDATE employees
SET salary = salary * 1.5;

SAVEPOINT B;

UPDATE employees
SET hire_date = sysdate
WHERE employee_id = 100;

ROLLBACK TO B;
ROLLBACK TO A;
ROLLBACK;

COMMIT;

INSERT INTO departments (department_id, department_name, location_id, manager_id)
VALUES (280,'Ventas',1700,100);

INSERT INTO departments
VALUES (290,'Bienestar',101,1700);

INSERT INTO departments (department_id, department_name)
VALUES (300,'Recreación');

INSERT INTO departments
VALUES (301,'Compras',NULL,NULL);

INSERT INTO employees
VALUES (207,'Peter','Parker','PPARKER','569.9999.0000',SYSDATE, 'AC_ACCOUNT',10000,NULL,100,90);

SELECT department_id FROM employees WHERE employee_id = 100;

INSERT INTO employees
VALUES(303,'Juan','Soto','JSOTO','569.8745.4523',SYSDATE,'AC_ACCOUNT',5000,NULL,100,(SELECT department_id FROM employees WHERE employee_id = 100));


SELECT MAX(employee_id)+1 FROM employees;

INSERT INTO employees
VALUES((SELECT MAX(employee_id)+1 FROM employees),
        'Tony','Stark','TSTARK','569.6598.3232',SYSDATE,'AC_ACCOUNT',10000,NULL,100,90);
        
SELECT department_name FROM departments
WHERE location_id=1700;

INSERT ALL
INTO copia_depto(nombre)
VALUES (department_name)
SELECT department_name FROM departments
WHERE location_id=1700;