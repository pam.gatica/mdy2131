INSERT ALL
    INTO bonif_arriendos_mensual
    VALUES(anno_mes,numrun_emp,nombre_empleado,sueldo_base,total_arriendos_mensual,bonif_por_arriendos)
SELECT
    TO_CHAR(sysdate,'YYYYMM') anno_mes,
    numrun_emp,
    INITCAP(e.pnombre_emp || ' ' || e.snombre_emp || ' ' || e.appaterno_emp || ' ' || e.apmaterno_emp) nombre_empleado,
    e.sueldo_base,
    COUNT(ac.id_arriendo) total_arriendos_mensual,
    ROUND(e.sueldo_base * COUNT(ac.id_arriendo) / 100) bonif_por_arriendos
FROM arriendo_camion ac
JOIN camion c USING (nro_patente)
JOIN empleado e USING(numrun_emp)
WHERE EXTRACT(YEAR FROM fecha_ini_arriendo) = EXTRACT(YEAR FROM sysdate)AND
EXTRACT(MONTH FROM fecha_ini_arriendo) = EXTRACT(MONTH FROM sysdate)
GROUP BY numrun_emp, e.pnombre_emp, e.snombre_emp, e.appaterno_emp, e.apmaterno_emp, e.sueldo_base
ORDER BY numrun_emp;