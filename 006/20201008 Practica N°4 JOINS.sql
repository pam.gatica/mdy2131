SELECT
    TO_CHAR(c.numrun,'99G999G999') || '-' || c.dvrun "RUN CLIENTE",
    INITCAP(c.pnombre  || ' ' || c.snombre || ' ' ||  c.appaterno || ' ' || c.apmaterno) "NOMBRE CLIENTE",
    TO_CHAR(c.fecha_nacimiento, 'dd "de" Month') "DIA DE CUMPLEAŅOS",
    co.nombre_comuna,
    sr.direccion || ' / ' ||  UPPER(r.nombre_region)"Direccion Sucursal / REGION SUCURSAL"
FROM cliente c
JOIN comuna co  ON (co.cod_comuna = c.cod_comuna AND co.cod_provincia = c.cod_provincia AND c.cod_region = co.cod_region)
JOIN sucursal_retail sr ON (co.cod_comuna = sr.cod_comuna AND co.cod_provincia = sr.cod_provincia AND co.cod_region = sr.cod_region)
JOIN region r ON (r.cod_region = sr.cod_region)
WHERE EXTRACT(MONTH FROM c.fecha_nacimiento) = EXTRACT(MONTH FROM sysdate)+1 AND
r.cod_region = &codigo_region;