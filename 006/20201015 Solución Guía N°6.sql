--1.Mostrar nombre, apellido y correo electr�nico 
--de todos los compa�eros de trabajo del empleado de apellido 
--Russell. (Los compa�eros de trabajo son aquellos que comparten el mismo cargo).

SELECT job_id FROM employees WHERE last_name = 'Russell';

SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = 'SA_MAN'
AND last_name <> 'Russell';

SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = (SELECT job_id FROM employees WHERE last_name = 'Russell')
AND last_name <> 'Russell';

/*
2.Mostrar nombre, apellido, sueldo, cargo de los empleados con sueldo superior al promedio de los sueldos.
*/

SELECT ROUND(AVG(salary)) FROM employees;

SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees 
NATURAL JOIN jobs
WHERE salary > 6574
ORDER BY salary;

SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees 
NATURAL JOIN jobs
WHERE salary > (SELECT ROUND(AVG(salary)) FROM employees)
ORDER BY salary;

/*
3.Mostrar id, nombre, apellido, email, tel�fono, cargo, departamento, direcci�n, ciudad y pa�s 
de los empleados con la mayor cantidad de a�os trabajados.
*/

SELECT MAX(ROUND(MONTHS_BETWEEN(sysdate,hire_date)/12)) FROM employees;

SELECT
    e.employee_id,
    e.first_name,
    e.last_name,
    e.email,
    e.phone_number,
    ROUND(MONTHS_BETWEEN(sysdate,e.hire_date)/12) "ANTIGUEDAD",
    j.job_title,
    d.department_name,
    l.street_address,
    l.city,
    c.country_name
FROM employees e
JOIN jobs j USING(job_id)
JOIN departments d ON(e.department_id = d.department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
WHERE ROUND(MONTHS_BETWEEN(sysdate,e.hire_date)/12) = 20;

SELECT
    e.employee_id,
    e.first_name,
    e.last_name,
    e.email,
    e.phone_number,
    j.job_title,
    d.department_name,
    l.street_address,
    l.city,
    c.country_name
FROM employees e
JOIN jobs j USING(job_id)
JOIN departments d ON(e.department_id = d.department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
WHERE ROUND(MONTHS_BETWEEN(sysdate,e.hire_date)/12) = (SELECT MAX(ROUND(MONTHS_BETWEEN(sysdate,hire_date)/12)) FROM employees);

/*4.Mostrar nombre del departamento y su sueldo promedio de todos los departamentos 
donde trabajen m�s del promedio de empleados. */


SELECT ROUND(AVG(COUNT(employee_id)))FROM employees GROUP BY department_id;

SELECT 
    d.department_name,
    ROUND(AVG(e.salary)),
    COUNT(e.employee_id)
FROM employees e
JOIN departments d ON (e.department_id = d.department_id)
GROUP BY d.department_name
HAVING COUNT(e.employee_id)> 9 ;

SELECT 
    d.department_name,
    ROUND(AVG(e.salary))
FROM employees e
JOIN departments d ON (e.department_id = d.department_id)
GROUP BY d.department_name
HAVING COUNT(e.employee_id)> (SELECT ROUND(AVG(COUNT(employee_id)))FROM employees GROUP BY department_id) ;

/*
5.Mostrar la cantidad de departamentos por pa�s cuyo sueldo promedio es mayor al promedio de los sueldos.
*/

SELECT ROUND(AVG(salary)) FROM employees;

SELECT
    c.country_name,
    COUNT(d.department_name),
    ROUND(AVG(e.salary))
FROM departments d
JOIN employees e ON(d.department_id = e.department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
GROUP BY c.country_name
HAVING ROUND(AVG(e.salary)) > 6574
ORDER BY c.country_name;

SELECT
    c.country_name,
    COUNT(d.department_name)
FROM departments d
JOIN employees e ON(d.department_id = e.department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
GROUP BY c.country_name
HAVING AVG(e.salary) > (SELECT AVG(salary) FROM employees)
ORDER BY c.country_name;