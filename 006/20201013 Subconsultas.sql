SELECT salary FROM employees WHERE last_name = 'Abel';

SELECT
    last_name,
    salary
FROM employees
WHERE salary > 11000
ORDER BY salary;


SELECT
    last_name,
    salary
FROM employees
WHERE salary > (SELECT salary FROM employees WHERE last_name = 'Abel')
ORDER BY salary;


SELECT MAX(salary) FROM employees;

SELECT
    last_name,
    salary
FROM employees
WHERE salary = 30000;

SELECT
    last_name,
    salary
FROM employees
WHERE salary = (SELECT MAX(salary) FROM employees);

SELECT job_id FROM employees WHERE employee_id = 141; --ST_CLERK
SELECT salary FROM employees WHERE employee_id = 143; -- 2600

SELECT
    last_name,
    job_id,
    salary
FROM employees
WHERE job_id = (SELECT job_id FROM employees WHERE employee_id = 141)
AND salary > (SELECT salary FROM employees WHERE employee_id = 143);


SELECT MIN(salary) FROM employees; --2100

SELECT
    last_name,
    salary
FROM employees
WHERE salary = (SELECT MIN(salary) FROM employees);

SELECT MIN(salary) FROM employees WHERE department_id= 80; --6100

SELECT
    department_id,
    MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) > (SELECT MIN(salary) FROM employees WHERE department_id= 80)
ORDER BY department_id;


SELECT salary
FROM employees
WHERE department_id = 30;

SELECT
    last_name,
    salary
FROM employees
WHERE salary > ALL(SELECT salary FROM employees WHERE department_id = 30);
SELECT
    last_name,
    salary
FROM employees
WHERE salary > (SELECT MAX(salary) FROM employees WHERE department_id = 30);


SELECT MIN(salary) FROM employees GROUP BY department_id;

SELECT
    last_name,
    salary
FROM employees
WHERE salary IN (SELECT MIN(salary) FROM employees GROUP BY department_id);

SELECT DISTINCT salary FROM employees WHERE job_id = 'IT_PROG';
SELECT
    last_name,
    salary
FROM employees
WHERE salary < ANY(SELECT DISTINCT salary FROM employees WHERE job_id = 'IT_PROG')
AND job_id <> 'IT_PROG';

CREATE TABLE EMPLEADOS_JEFE AS
SELECT  
    e.employee_id "ID_EMPLEADO",
    e.first_name || ' ' || e.last_name "NOMBRE_EMPLEADO",
    CASE
        WHEN e.manager_id IS NULL THEN 'Sin Jefe'
        WHEN e.manager_id IS NOT NULL THEN m.first_name || ' ' || m.last_name
    END
    "NOMBRE_JEFE"
FROM employees e
LEFT JOIN employees m ON (e.manager_id = m.employee_id)
ORDER BY e.employee_id;