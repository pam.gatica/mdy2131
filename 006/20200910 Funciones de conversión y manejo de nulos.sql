SELECT sysdate+20 FROM dual;

SELECT last_name, hire_date, sysdate, sysdate-hire_date FROM employees;

SELECT last_name, hire_date 
FROM employees
WHERE extract(month FROM hire_date) IN (4,5)
ORDER BY extract(month FROM hire_date), extract(year FROM hire_date);

SELECT first_name, hire_date,INITCAP(TO_CHAR(hire_date,'MONTH')), salary, TO_CHAR(salary,'$999G999')
FROM employees;

SELECT TO_CHAR(TO_DATE('2020/09/10','YYYY/MM/DD'),'CC') FROM dual;

SELECT last_name, salary , commission_pct , NVL2(commission_pct, salary*commission_pct,salary) 
FROM employees;