GRANT CREATE TABLE TO DESARROLLO_PERSONAL;

GRANT SELECT
ON hrcloud.employees
TO DESARROLLO_PERSONAL;

CREATE ROLE rol_gerencia;

GRANT CREATE TABLE, CREATE VIEW TO rol_gerencia;
GRANT SELECT, INSERT, UPDATE, DELETE ON hrcloud.employees TO rol_gerencia;


GRANT rol_gerencia TO administrador;
ALTER USER administrador DEFAULT ROLE "ROL_GERENCIA";

REVOKE SELECT
ON hrcloud.employees
FROM DESARROLLO_PERSONAL;