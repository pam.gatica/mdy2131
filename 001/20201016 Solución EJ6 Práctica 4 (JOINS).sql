CREATE TABLE atenciones2019 AS
SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = 2019;

SELECT
    p.pnombre,
    p.apaterno,
    COUNT(a.ate_id)
FROM paciente p
LEFT JOIN (SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = 2019) a USING(pac_rut)
GROUP BY pac_rut, p.pnombre, p.apaterno
ORDER BY p.apaterno, p.pnombre;

SELECT
    p.pnombre,
    p.apaterno,
    COUNT(a.ate_id)
FROM paciente p
LEFT JOIN atenciones2019 a USING(pac_rut)
GROUP BY pac_rut, p.pnombre, p.apaterno
ORDER BY p.apaterno, p.pnombre;

DROP TABLE atenciones2019;