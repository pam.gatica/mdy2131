SELECT
    e.last_name,
    j.job_title
FROM employees e
NATURAL JOIN jobs j;

SELECT
    e.last_name,
    d.department_name
FROM employees e
JOIN departments d USING (department_id);

SELECT
    e.last_name,
    d.department_name
FROM employees e
JOIN departments d ON (d.department_id = e.department_id);

SELECT
    d.department_name,
    l.city,
    c.country_name,
    r.region_name
FROM departments d
JOIN locations l USING (location_id)
JOIN countries c USING (country_id)
JOIN regions r USING (region_id);

SELECT
    e.employee_id "ID EMPLEADO",
    e.first_name || ' '  || e.last_name "NOMBRE EMPLEADO",
    e.manager_id "ID JEFE",
    j.first_name || ' ' || j.last_name "NOMBRE JEFE"
FROM employees e
JOIN employees j ON (e.manager_id = j.employee_id);

SELECT
    e.employee_id,
    e.first_name,
    NVL(e.department_id,0),
    NVL(d.department_name,'Sin Departamento')
FROM employees e
LEFT JOIN departments d ON (d.department_id = e.department_id)
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    e.first_name,
    NVL(e.department_id,0),
    NVL(d.department_name,'Sin Departamento')
FROM employees e
RIGHT JOIN departments d ON (d.department_id = e.department_id)
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    e.first_name,
    NVL(e.department_id,0),
    NVL(d.department_name,'Sin Departamento')
FROM employees e
FULL JOIN departments d ON (d.department_id = e.department_id)
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    d.department_id,
    d.department_name
FROM employees e
CROSS JOIN departments d
ORDER BY e.employee_id;

SELECT
    d.department_name,
    COUNT(e.employee_id),
    l.city
FROM employees e
JOIN departments d USING(department_id)
JOIN locations l USING(location_id)
GROUP BY d.department_name,l.city;