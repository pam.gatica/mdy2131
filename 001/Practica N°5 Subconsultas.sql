--*****************************************************
--EJERCICIO 1
--SUBQUERY atenciones del 2019
SELECT ate_id,med_rut FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)-1;
--SUBQUERY m�ximo de atenciones por m�dico efectuadas el 2019
SELECT MAX(COUNT(a.ate_id))FROM medico m LEFT JOIN (SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)-1) a USING(med_rut)
GROUP BY med_rut;
--CONSULTA PRINCIPAL
CREATE TABLE MEDICOS_SERVICIO_COMUNIDAD AS
SELECT
    u.nombre "UNIDAD",
    m.pnombre ||  ' ' || m.apaterno || ' ' || m.amaterno "MEDICO",
    substr(u.nombre,1,2)||substr(m.apaterno,length(m.apaterno)-2,2)|| substr(TO_CHAR(m.telefono),1,3)||TO_CHAR(m.fecha_contrato,'YYYY')||'@medicocktk.cl' "CORREO MEDICO",
    COUNT(a.ate_id) "ATENCIONES MEDICAS"
FROM medico m
LEFT JOIN (SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)-1) a USING(med_rut)
JOIN unidad u USING (uni_id)
GROUP BY med_rut, m.pnombre, m.apaterno, m.amaterno, u.nombre, m.telefono,m.fecha_contrato
HAVING COUNT(a.ate_id) < (SELECT MAX(COUNT(a.ate_id))FROM medico m LEFT JOIN (SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)-1) a USING(med_rut)
GROUP BY med_rut)
ORDER BY u.nombre,m.apaterno;


--EJERCICIO 1 (FORMA ALTERNATIVA)
CREATE TABLE atenciones2019 AS 
SELECT ate_id,med_rut FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)-1;
--CONSULTA PRINCIPAL
CREATE TABLE MEDICOS_SERVICIO_COMUNIDAD AS
SELECT
    u.nombre "UNIDAD",
    m.pnombre ||  ' ' || m.apaterno || ' ' || m.amaterno "MEDICO",
    substr(u.nombre,1,2)||substr(m.apaterno,length(m.apaterno)-2,2)|| substr(TO_CHAR(m.telefono),1,3)||TO_CHAR(m.fecha_contrato,'YYYY')||'@medicocktk.cl' "CORREO MEDICO",
    COUNT(a.ate_id) "ATENCIONES MEDICAS"
FROM medico m
LEFT JOIN atenciones2019 a USING(med_rut)
JOIN unidad u USING (uni_id)
GROUP BY med_rut, m.pnombre, m.apaterno, m.amaterno, u.nombre, m.telefono,m.fecha_contrato
HAVING COUNT(a.ate_id) < (SELECT MAX(COUNT(a.ate_id))FROM medico m LEFT JOIN atenciones2019 a USING(med_rut)
GROUP BY med_rut)
ORDER BY u.nombre,m.apaterno;

DROP TABLE atenciones2019;

--*********************************************************************
-- EJERCICIO 2.1
--SUBQUERY promedio de atenciones m�dicas mensuales
SELECT AVG(COUNT(ate_id)) FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)  GROUP BY EXTRACT(MONTH FROM fecha_atencion);
--CONSULTA PRINCIPAL
SELECT
    TO_CHAR(EXTRACT(MONTH FROM fecha_atencion),'09')||'/'||EXTRACT(YEAR FROM fecha_atencion)"MES Y A�O",
    COUNT(ate_id)"TOTAL ATENCIONES",
    TO_CHAR(SUM(costo),'$999G999G999') "VALOR TOTAL DE LAS ATENCIONES"
FROM atencion
WHERE TO_CHAR(fecha_atencion,'YYYY') = TO_CHAR(sysdate,'YYYY')
GROUP BY EXTRACT(MONTH FROM fecha_atencion),EXTRACT(YEAR FROM fecha_atencion)
HAVING COUNT(ate_id) > (SELECT AVG(COUNT(ate_id)) FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion)=EXTRACT(YEAR FROM sysdate)  GROUP BY EXTRACT(MONTH FROM fecha_atencion)) 
ORDER BY EXTRACT(MONTH FROM fecha_atencion);

-- EJERCICIO 2.2
--SUBQUERY Promedio de d�as de morosidad anual
SELECT AVG(pa.fecha_pago - pa.fecha_venc_pago)FROM pago_atencion pa WHERE pa.fecha_pago - pa.fecha_venc_pago > 0 AND EXTRACT(YEAR FROM pa.fecha_venc_pago) = EXTRACT(YEAR FROM sysdate);

SELECT
    p.pac_rut||'-'||p.dv_rut "RUT PACIENTE",
    p.pnombre || ' ' || p.snombre ||' '||p.apaterno|| ' ' || p.amaterno AS "NOMBRE PACIENTE",
    a.ate_id "ID ATENCION",
    pa.fecha_venc_pago "FECHA VENCIMIENTO PAGO",
    pa.fecha_pago "FECHA PAGO",
    pa.fecha_pago - pa.fecha_venc_pago "DIAS MOROSIDAD",
    TO_CHAR((pa.fecha_pago - pa.fecha_venc_pago)*2000,'$999G999') "VALOR MULTA"
FROM paciente p
    JOIN atencion a ON (p.pac_rut = a.pac_rut)
    JOIN pago_atencion pa ON (a.ate_id = pa.ate_id)
WHERE   pa.fecha_pago - pa.fecha_venc_pago > 0 
    AND     EXTRACT(YEAR FROM pa.fecha_venc_pago) = EXTRACT(YEAR FROM sysdate)
    AND     pa.fecha_pago - pa.fecha_venc_pago > (SELECT AVG(pa.fecha_pago - pa.fecha_venc_pago)FROM pago_atencion pa WHERE pa.fecha_pago - pa.fecha_venc_pago > 0 AND EXTRACT(YEAR FROM pa.fecha_venc_pago) = EXTRACT(YEAR FROM sysdate)) 
ORDER BY pa.fecha_venc_pago, "DIAS MOROSIDAD" DESC;


--********************************************
-- EJERCICIO 3

--SUBQUERY atenciones del mes pasado
SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = EXTRACT(YEAR FROM sysdate) AND EXTRACT(MONTH FROM fecha_atencion) = EXTRACT(MONTH FROM sysdate)-1;
-- SUBQUERY atenciones promedio diarias del mes pasado
SELECT AVG(COUNT(ate_id)) FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = EXTRACT(YEAR FROM sysdate) AND EXTRACT(MONTH FROM fecha_atencion) = EXTRACT(MONTH FROM sysdate)-1 GROUP BY fecha_atencion;

SELECT 
     ts.descripcion||', ' ||s.descripcion "SISTEMA_SALUD",
     COUNT(a.ate_id)
FROM (SELECT * FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = EXTRACT(YEAR FROM sysdate) AND EXTRACT(MONTH FROM fecha_atencion) = EXTRACT(MONTH FROM sysdate)-1) a
JOIN paciente p USING (pac_rut)
JOIN salud s USING(sal_id)
JOIN tipo_salud ts USING(tipo_sal_id)
WHERE tipo_sal_id = 'F' OR tipo_sal_id = 'I'
GROUP BY sal_id,ts.descripcion,s.descripcion
HAVING COUNT(a.ate_id) > (SELECT AVG(COUNT(ate_id)) FROM atencion WHERE EXTRACT(YEAR FROM fecha_atencion) = EXTRACT(YEAR FROM sysdate) AND EXTRACT(MONTH FROM fecha_atencion) = EXTRACT(MONTH FROM sysdate)-1 GROUP BY fecha_atencion)
ORDER BY sal_id,s.descripcion;