/*
identificaci�n de la atenci�n, run del paciente, nombre completo, a�os que tiene, 
el costo de la atenci�n sin descuento, el costo de la atenci�n con descuento y 
el monto a devolver por cada atenci�n del paciente.

*/

SELECT
    a.ate_id,
    TO_CHAR(pac_rut,'99G999G999') || '-' || p.dv_rut "RUT PACIENTE",
    p.pnombre || ' ' || p.snombre || ' ' || p.apaterno || ' ' || p.amaterno "NOMBRE PACIENTE",
    EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM p.fecha_nacimiento) "A�OS",
    a.fecha_atencion || ' ' || a.hr_atencion "FECHA Y HORA ATENCION",
    TO_CHAR(a.costo,'$999G999') "COSTO ATENCION S/DESCUENTO",
    TO_CHAR(a.costo-(a.costo*po.porcentaje_descto/100),'$999G999') "COSTO ATENCION C/DESCUENTO",
     TO_CHAR(a.costo -(a.costo-(a.costo*po.porcentaje_descto/100)),'$999G999') "MONTO A DEVOLVER"
FROM paciente p
JOIN atencion a USING(pac_rut) --JOIN atencion a ON(p.pac_rut = a.pac_rut)
JOIN porc_descto_3ra_edad po ON EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM p.fecha_nacimiento) BETWEEN po.anno_ini AND po.anno_ter
WHERE  EXTRACT(YEAR FROM sysdate) - EXTRACT(YEAR FROM p.fecha_nacimiento) >= 65
ORDER BY a.fecha_atencion, "A�OS" DESC; 

--SUBCONSULTAS (HR)

SELECT
    *
FROM employees
WHERE salary >  11000 ;

SELECT
    *
FROM employees
WHERE salary >  ( SELECT salary FROM employees WHERE last_name = 'Abel'  ) ;

SELECT
    salary
FROM employees
WHERE last_name = 'Abel';

SELECT MAX(salary) FROM employees;

SELECT
 *
FROM employees
WHERE salary = 30000;

SELECT
 *
FROM employees
WHERE salary = (SELECT MAX(salary) FROM employees);

SELECT
   salary
FROM employees
WHERE department_id = 30;

SELECT
    *
FROM employees
WHERE salary > (SELECT MAX(salary) FROM employees WHERE department_id = 30);

SELECT
    *
FROM employees
WHERE salary > ALL(SELECT salary FROM employees WHERE department_id = 30);

SELECT
    *
FROM employees
WHERE salary > ANY(SELECT salary FROM employees WHERE department_id = 30);

SELECT
    MIN(salary)
FROM employees
GROUP BY department_id;

SELECT
    last_name, salary
FROM employees
WHERE salary IN (SELECT MIN(salary)FROM employees GROUP BY department_id);

SELECT department_id, department_name
FROM departments d
WHERE EXISTS (SELECT department_id FROM employees e WHERE e.department_id = d.department_id);

SELECT department_id, department_name
FROM departments d
WHERE EXISTS (SELECT department_id FROM employees e WHERE e.employee_id = 100);

SELECT
    e.employee_id,
    e.first_name || ' ' || e.last_name "NOMBRE_EMPLEADO",
    CASE 
        WHEN j.manager_id IS NULL THEN 'SIN JEFE'
        WHEN j.manager_id IS NOT NULL THEN j.first_name || ' ' || j.last_name
    END "NOMBRE_JEFE"
FROM employees e
LEFT JOIN employees j ON (j.employee_id = e.manager_id)
ORDER BY e.employee_id;




