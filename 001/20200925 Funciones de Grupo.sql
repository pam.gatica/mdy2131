SELECT
    ROUND(AVG(salary)) "Sueldo Promedio",
    SUM(salary) "Total Sueldos",
    MAX(salary) "Sueldo M�ximo",
    MIN(salary) "Sueldo M�nimo"
FROM employees;

SELECT
    *
FROM employees;

SELECT
    COUNT(*) "Total Empleados",
    COUNT(commission_pct)"Empleados a comisi�n",
    COUNT(DISTINCT department_id) "Cantidad de departamentos"
FROM employees;

SELECT
    COUNT(employee_id),
    COUNT(commission_pct)
FROM employees
WHERE department_id = 50;

SELECT
    department_id,
    ROUND(AVG(salary))
FROM employees
GROUP BY department_id
ORDER BY AVG(salary);

SELECT
    'En el departamento ' ||
    NVL(TO_CHAR(department_id),'NULO') ||
    ' trabajan ' ||
    COUNT(employee_id) ||
    ' empleados'
FROM employees
GROUP BY department_id
ORDER BY department_id;

SELECT
    department_id,
    MAX(salary)
FROM employees
GROUP BY department_id
HAVING MAX(salary) > 5000;