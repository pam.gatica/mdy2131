--1.Mostrar nombre, apellido y correo electr�nico de todos 
--los compa�eros de trabajo del empleado de apellido Russell. (Los compa�eros de trabajo son aquellos que comparten el mismo cargo).

--SUBQUERY: Cargo del empleado de apellido Russell
SELECT job_id FROM employees WHERE last_name = 'Russell';

--MAINQUERY:
SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = 'SA_MAN'
AND last_name <> 'Russell';

SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = (SELECT job_id FROM employees WHERE last_name = 'Russell')
AND last_name <> 'Russell';

--2.Mostrar nombre, apellido, sueldo, cargo de los empleados con sueldo 
--superior al promedio de los sueldos.

--SUBQUERY: promedio de los sueldos de todos los empleados
SELECT AVG(salary) FROM employees;

--MAINQUERY:
SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees
NATURAL JOIN jobs
WHERE salary > 6574;

SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees
NATURAL JOIN jobs
WHERE salary > (SELECT AVG(salary) FROM employees);


--3.Mostrar id, nombre, apellido, email, tel�fono, cargo, departamento, 
--direcci�n, ciudad y pa�s de los empleados con 
--la mayor cantidad de a�os trabajados.}

--SUBQUERY: Mayor cantidad de a�os trabajados
SELECT MAX(TRUNC(MONTHS_BETWEEN(sysdate,hire_date)/12)) FROM employees;

--MAINQUERY:
    SELECT
        e.employee_id,
        e.first_name,
        e.last_name,
        e.email,
        e.phone_number,
        job_title,
        d.department_name,
        street_address,
        city,
        country_name
    FROM employees e
    NATURAL JOIN jobs
    JOIN departments d USING(department_id)
    JOIN locations USING(location_id)
    JOIN countries USING(country_id)
    WHERE TRUNC(MONTHS_BETWEEN(sysdate,e.hire_date)/12) = 19;
    
        SELECT
        e.employee_id,
        e.first_name,
        e.last_name,
        e.email,
        e.phone_number,
        job_title,
        d.department_name,
        street_address,
        city,
        country_name
    FROM employees e
    NATURAL JOIN jobs
    JOIN departments d USING(department_id)
    JOIN locations USING(location_id)
    JOIN countries USING(country_id)
    WHERE TRUNC(MONTHS_BETWEEN(sysdate,e.hire_date)/12) = (SELECT MAX(TRUNC(MONTHS_BETWEEN(sysdate,hire_date)/12)) FROM employees);
    
--4.Mostrar nombre del departamento y su sueldo promedio de 
--todos los departamentos donde trabajen m�s del promedio 
--de empleados. 

--SUBQUERY: cu�ntos empleados trabajan en cada departamento y cual es el promedio
SELECT AVG(COUNT(employee_id)) FROM employees GROUP BY department_id;

--MAINQUERY:
SELECT
    d.department_name,
    ROUND(AVG(e.salary)),
    COUNT(e.employee_id)
FROM employees e
JOIN departments d USING (department_id)
GROUP BY d.department_name;

SELECT
    d.department_name,
    ROUND(AVG(e.salary))
FROM employees e
JOIN departments d USING (department_id)
GROUP BY d.department_name
HAVING COUNT(e.employee_id) > 8.9;

SELECT
    d.department_name,
    ROUND(AVG(e.salary))
FROM employees e
JOIN departments d USING (department_id)
GROUP BY d.department_name
HAVING COUNT(e.employee_id) > (SELECT AVG(COUNT(employee_id)) FROM employees GROUP BY department_id);


--5.Mostrar la cantidad de departamentos por pa�s cuyo sueldo promedio es mayor al promedio de los sueldos.

--SUBQUERY: promedio de los sueldos
SELECT AVG(salary) FROM employees;

--MAINQUERY:
SELECT
    c.country_name,
    COUNT(DISTINCT d.department_name)
FROM departments d
JOIN employees e USING(department_id)
JOIN locations USING(location_id)
JOIN countries c USING(country_id)
GROUP BY c.country_name
HAVING AVG(e.salary) > 6574;

SELECT
    c.country_name,
    COUNT(DISTINCT d.department_name)
FROM departments d
JOIN employees e USING(department_id)
JOIN locations USING(location_id)
JOIN countries c USING(country_id)
GROUP BY c.country_name
HAVING AVG(e.salary) > (SELECT AVG(salary) FROM employees);