--Privilegios

GRANT SELECT
ON hrcloud.employees
TO desarrollo_personal;

SELECT *
FROM user_role_privs;

SELECT *
FROM user_sys_privs;

REVOKE SELECT ON hrcloud.employees
FROM desarrollo_personal;