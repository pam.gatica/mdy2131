--PARTE 1
INSERT ALL
    INTO puntos_boleta (numero_boleta, puntaje)
    VALUES (numero_boleta, puntaje)
SELECT
    numero_boleta,
    ROUND((SUM(precio_venta * cantidad))/1000) puntaje
FROM boleta
JOIN pedido USING (cod_pedido)
JOIN carro USING (cod_carro)
JOIN producto_carro USING (cod_carro)
WHERE EXTRACT(YEAR FROM fecha_creacion) = EXTRACT(YEAR FROM sysdate) 
GROUP BY numero_boleta
ORDER BY numero_boleta;

--PARTE 2
--Poblado tabla puntos_cliente
INSERT ALL
    INTO puntos_cliente (periodo, run, puntaje_periodo)
    VALUES (TO_CHAR(sysdate,'YYYY'),run,puntaje_periodo)
SELECT
    run,
    NVL(SUM(puntaje),0) puntaje_periodo
FROM puntos_boleta
JOIN (SELECT * FROM boleta WHERE EXTRACT(YEAR FROM fecha_emision) = EXTRACT(YEAR FROM sysdate) ) USING(numero_boleta)
JOIN pedido USING(cod_pedido)
JOIN carro USING (cod_carro)
FULL JOIN cliente USING (run)
GROUP BY run
ORDER BY run;

-- Poblado tabla categoria_cliente
INSERT ALL
    INTO categoria_cliente (periodo, run,categoria,descuento)
    VALUES (EXTRACT(YEAR FROM sysdate)+1,run,categoria, descuento)
SELECT
    run,
    categoria,
    descuento
FROM puntos_cliente
JOIN categoria ON (puntaje_periodo BETWEEN puntaje_desde AND puntaje_hasta)
WHERE periodo = TO_CHAR(sysdate,'YYYY')
ORDER BY run;

--PARTE 3
CREATE SEQUENCE corr_promo
START WITH 1
INCREMENT BY 1;

INSERT ALL 
    INTO promo_carritos(correlativo,run,fecha_inicio_promo, fecha_fin_promo, descuento)
    VALUES (CORR_PROMO.nextval,run,TRUNC(sysdate,'MM'),LAST_DAY(sysdate),descuento+15)
SELECT
    run,
    descuento
FROM carro
JOIN categoria_cliente USING (run)
WHERE cod_carro IN (SELECT
                        cod_carro
                    FROM carro
                    WHERE EXTRACT(MONTH FROM fecha_creacion) = EXTRACT(MONTH FROM sysdate)-1
                    MINUS
                    SELECT
                        cod_carro
                    FROM pedido
                    WHERE EXTRACT(MONTH FROM fecha_pedido) = EXTRACT(MONTH FROM sysdate)-1);
                    
INSERT ALL 
    INTO promo_carritos(correlativo,run,fecha_inicio_promo, fecha_fin_promo, descuento)
    VALUES (CORR_PROMO.nextval,run,TRUNC(sysdate,'MM'),LAST_DAY(sysdate),descuento+15)
SELECT
run,
descuento
FROM carro
LEFT JOIN pedido USING (cod_carro)
JOIN categoria_cliente USING (run)
WHERE EXTRACT(MONTH FROM fecha_creacion) = EXTRACT(MONTH FROM sysdate)-1 AND cod_pedido IS NULL ;


-- PARTE 4
INSERT INTO pedido(cod_pedido, fecha_pedido, direccion_envio, cod_carro)
VALUES (
    (SELECT MAX(cod_pedido) +1 FROM pedido),
    sysdate,
    'Alvarez 2336,Vi�a del Mar',
    (SELECT cod_carro FROM carro WHERE fecha_creacion = '07/10/2020' and run = (SELECT run FROM cliente WHERE nombre='Kalindi' AND apellido ='Pettisall'))
    );
 
INSERT INTO estado(cod_estado, cod_pedido, fecha_estado)
VALUES(
    'EP',
    (SELECT MAX(cod_pedido) FROM pedido),
    sysdate
);


-- PARTE 5
UPDATE producto SET precio = ROUND(precio - precio * 0.1)
WHERE cod_producto = (  SELECT cod_producto  FROM producto_carro
                        WHERE cod_carro NOT IN (SELECT cod_carro FROM carro MINUS SELECT cod_carro FROM pedido)
                        GROUP BY cod_producto
                        HAVING sum(cantidad) = (SELECT MAX(SUM(cantidad))FROM producto_carro 
                                                WHERE cod_carro NOT IN (SELECT cod_carro FROM carro MINUS SELECT cod_carro FROM pedido)
                                                GROUP BY cod_producto)
                        );

SELECT * FROM producto 
WHERE cod_producto = (  SELECT cod_producto 
                        FROM producto_carro
                        WHERE cod_carro NOT IN (SELECT cod_carro FROM carro MINUS SELECT cod_carro FROM pedido)
                        GROUP BY cod_producto
                        HAVING sum(cantidad) = (SELECT MAX(SUM(cantidad))
                                                FROM producto_carro 
                                                WHERE cod_carro NOT IN (SELECT cod_carro FROM carro MINUS SELECT cod_carro FROM pedido)
                                                GROUP BY cod_producto)
                    );


