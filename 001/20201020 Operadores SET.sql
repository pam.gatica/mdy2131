CREATE TABLE emp_sal_12000 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE salary > 12000;

CREATE TABLE emp_depto_90 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE department_id = 90;

CREATE TABLE emp_depto_100 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE department_id = 100;

SELECT employee_id, salary, department_id FROM emp_sal_12000
UNION
SELECT employee_id, salary, department_id FROM emp_depto_90;

SELECT employee_id id_empleado, salary sueldo FROM emp_depto_100
UNION
SELECT employee_id , salary  FROM emp_sal_12000;


SELECT employee_id, first_name || ' ' || last_name "NOMBRE_EMPLEADO"
FROM emp_sal_12000
UNION
SELECT employee_id, first_name || ' ' || last_name "NOMBRE_EMPLEADO"
FROM emp_depto_90
UNION
SELECT employee_id, first_name || ' ' || last_name "NOMBRE_EMPLEADO"
FROM emp_depto_100
ORDER BY employee_id;

SELECT employee_id, salary*0.01, department_id FROM emp_sal_12000
UNION ALL
SELECT employee_id, salary*0.1, department_id FROM emp_depto_90
ORDER BY employee_id;

SELECT employee_id, salary, department_id FROM emp_sal_12000
INTERSECT
SELECT employee_id, salary, department_id FROM emp_depto_90;

SELECT employee_id, salary, department_id FROM emp_sal_12000
INTERSECT
SELECT employee_id, salary, department_id FROM emp_depto_90
INTERSECT
SELECT employee_id, salary, department_id FROM emp_depto_100;

SELECT employee_id, salary, department_id FROM emp_sal_12000
MINUS
SELECT employee_id, salary, department_id FROM emp_depto_90;

SELECT employee_id, job_id, salary FROM employees
UNION ALL
SELECT employee_id, job_id,0 FROM job_history
ORDER BY employee_id;

SELECT e.employee_id, job_id
FROM employees e
JOIN job_history jh USING(job_id)
ORDER BY e.employee_id;

SELECT employee_id, first_name, salary FROM employees
MINUS
SELECT employee_id, first_name, salary FROM emp_sal_12000
ORDER BY 2,3;