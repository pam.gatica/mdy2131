SELECT last_name, hire_date
FROM employees
WHERE EXTRACT(MONTH FROM hire_date) IN(&mes)
ORDER BY EXTRACT(MONTH FROM hire_date), hire_date;

SELECT 
    sysdate,
    TO_CHAR(sysdate,'Month "de" yyyy') Mes,
    TO_CHAR(sysdate,'YYYY')A�o
FROM dual;
/*DD/MM/YYYY*/
SELECT TO_DATE('2020/12/31','YYYY/MM/DD')
FROM dual;

SELECT  last_name, salary,
        NVL(commission_pct,0),
        NVL2(commission_pct,CONCAT('0',TO_CHAR(commission_pct)),'SC')
FROM employees;