CREATE VIEW v_emp_depto_80 AS
SELECT employee_id, last_name, salary
FROM employees
WHERE department_id = 80;

SELECT * FROM v_emp_depto_80
WHERE salary > 10000;

SELECT * FROM employees WHERE last_name = 'Russell';

DESC v_emp_depto_80;
DESC departments;

CREATE OR REPLACE VIEW v_salarios_depto_50 AS
SELECT  employee_id id_empleado,
        last_name apellido,
        salary sueldo, 
        salary*12 sueldo_anual
FROM employees
WHERE department_id = 50
WITH READ ONLY;

SELECT * FROM v_salarios_depto_50;
DESC v_salarios_depto_50;

DELETE FROM v_salarios_depto_50;

CREATE VIEW v_dept_sum 
(nombre_depto,minsal,maxsal,avgsal)AS
SELECT 
    d.department_name,
    MIN(e.salary),
    MAX(e.salary),
    ROUND(AVG(e.salary))
FROM employees e
JOIN departments d ON (e.department_id = d.department_id)
GROUP BY d.department_name;

SELECT * FROM v_dept_sum
ORDER BY minsal;

DROP VIEW v_dept_sum;

DROP SEQUENCE seq_dept_id;

CREATE SEQUENCE seq_dept_id
INCREMENT BY 10
START WITH 280
MAXVALUE 9999;

SELECT seq_dept_id.CURRVAL FROM dual;

INSERT INTO departments 
VALUES (seq_dept_id.NEXTVAL, 'Support',NULL,1700);

INSERT INTO departments 
VALUES (seq_dept_id.NEXTVAL, 'Bienestar',NULL,1700);

CREATE OR REPLACE SYNONYM syn_deptos
FOR deparments;
