
SELECT * FROM employees;

DELETE FROM employees WHERE department_id IS NULL;

SAVEPOINT A;

UPDATE employees SET salary = salary *1.5;

SAVEPOINT B;

DELETE FROM employees WHERE manager_id IS NULL;

ROLLBACK;
ROLLBACK TO B;
COMMIT;

SELECT * FROM departments;

INSERT INTO departments (department_id, department_name, manager_id, location_id)
VALUES (280,'Relaciones Publicas', 204,1700);

INSERT INTO departments (department_id, department_name)
VALUES(290,'Ventas');

INSERT INTO departments ( department_id)
VALUES (300);

INSERT INTO departments 
VALUES(300,'Comunicaciones',NULL,NULL);


SELECT department_id FROM employees WHERE employee_id = 100;

SELECT * FROM employees;

INSERT INTO employees
VALUES(303,'Juan','Soto','JSOTO','515.124.4567',SYSDATE,'AC_ACCOUNT',6900,NULL,205,(SELECT department_id FROM employees WHERE employee_id = 100));

SELECT MAX(employee_id)+1
FROM employees;

INSERT INTO employees
VALUES((SELECT MAX(employee_id)+1 FROM employees),'Rosa','Garrido',USER,'123456789',SYSDATE,'AC_ACCOUNT',10000,NULL,205,90);


CREATE TABLE copia_depto(
id number(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999 INCREMENT BY 1 START WITH 1,
nombre VARCHAR2(30) NOT NULL);


SELECT department_name "NOMBRE_DEPTO" FROM departments WHERE location_id = 1700;

INSERT ALL
    INTO copia_depto (nombre)
    VALUES (nombre_depto)
SELECT department_name "NOMBRE_DEPTO" FROM departments WHERE location_id = 1700;   
SAVEPOINT C;

UPDATE employees
SET salary = salary * 2;

UPDATE employees
SET salary = salary * 2
WHERE employee_id = 165;

ROLLBACK TO C;