CREATE TABLE emp_sal_12000 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE salary > 12000;

CREATE TABLE emp_depto_90 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE department_id = 90;

CREATE TABLE emp_depto_100 AS
SELECT 
    employee_id,
    first_name,
    last_name,
    salary,
    department_id
FROM employees
WHERE department_id = 100;


SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000
UNION
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_90
UNION
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100;

SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000
UNION ALL
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_90
UNION ALL
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100;

SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000
INTERSECT
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_90;

SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000
INTERSECT
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100;

SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_90
INTERSECT
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100;

SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000
INTERSECT
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_90
INTERSECT
SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100;

SELECT employee_id, first_name || ' ' || last_name FROM emp_depto_100
MINUS
SELECT employee_id, first_name || ' ' || last_name FROM emp_sal_12000;


SELECT employee_id "ID EMPLEADO", job_id, TO_CHAR(salary) "SUELDO" FROM employees
UNION
SELECT employ1ee_id, job_id,'0' "SALARIO"  FROM job_history
ORDER BY "SUELDO";

SELECT employee_id "ID EMPLEADO", job_id, TO_CHAR(salary) "SUELDO" FROM employees
UNION
SELECT employee_id, job_id,'0' "SALARIO"  FROM job_history
ORDER BY 3,1;