SELECT
    emp.employee_id "ID EMPLEADO",
    emp.first_name || ' ' || emp.last_name "NOMBRE EMPLEADO",
    emp.manager_id "ID JEFE",
    jefe.first_name || ' ' || jefe.last_name "NOMBRE JEFE"
FROM employees emp
JOIN employees jefe ON jefe.employee_id = emp.manager_id
ORDER BY emp.employee_id;

SELECT
    jefe.employee_id "ID JEFE",
    jefe.first_name || ' ' || jefe.last_name "NOMBRE JEFE",
    COUNT( emp.employee_id) "EMPLEADOS A CARGO"
FROM employees emp
JOIN employees jefe ON jefe.employee_id = emp.manager_id
GROUP BY jefe.employee_id, jefe.first_name, jefe.last_name
ORDER BY jefe.employee_id;

--PARA EJEMPLO NONEQUIJOIN DEL PPT
CREATE TABLE PORC_BONO_ANNOS (
ANNO_INI NUMBER,
ANNO_TER NUMBER,
PORCENTAJE_BONO NUMBER);

INSERT INTO PORC_BONO_ANNOS VALUES (1,5,5);
INSERT INTO PORC_BONO_ANNOS VALUES (6,10,7);
INSERT INTO PORC_BONO_ANNOS VALUES (11,15,10);
INSERT INTO PORC_BONO_ANNOS VALUES (16,20,12);
INSERT INTO PORC_BONO_ANNOS VALUES (21,25,15);
INSERT INTO PORC_BONO_ANNOS VALUES (26,30,20);
INSERT INTO PORC_BONO_ANNOS VALUES (31,40,30);

SELECT
    e.employee_id,
    e.salary,
    ROUND(MONTHS_BETWEEN(sysdate,e.hire_date)/12) "ANTIGUEDAD",
    pb.porcentaje_bono*e.salary/100 "BONO"
FROM employees e
JOIN porc_bono_annos pb ON ROUND(MONTHS_BETWEEN(sysdate,e.hire_date)/12) BETWEEN pb.anno_ini AND pb.anno_ter
ORDER BY ANTIGUEDAD;


SELECT
    e.employee_id,
    e.first_name
    department_id,
    d.department_name
FROM employees e
JOIN departments d USING(department_id);

SELECT
    e.employee_id,
    e.first_name
    department_id,
    NVL(d.department_name,'Sin depto asociado')
FROM employees e
LEFT JOIN departments d USING(department_id)
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    e.first_name
    department_id,
    NVL(d.department_name,'Sin depto asociado')
FROM employees e
RIGHT JOIN departments d USING(department_id)
ORDER BY e.employee_id;

SELECT
    e.employee_id,
    e.first_name
    department_id,
    NVL(d.department_name,'Sin depto asociado')
FROM employees e
FULL JOIN departments d USING(department_id)
ORDER BY e.employee_id;

SELECT
    department_id,
    d.department_name,
    COUNT(e.employee_id)
FROM employees e
RIGHT JOIN departments d USING(department_id)
GROUP BY department_id, d.department_name
ORDER BY department_id;

SELECT
    e.employee_id,
    d.department_id
FROM employees e
CROSS JOIN departments d;

SELECT
    e.employee_id,
    d.department_name,
    l.city,
    c.country_name,
    r.region_name
FROM employees e
JOIN departments d USING(department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
JOIN regions r USING(region_id);