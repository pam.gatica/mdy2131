SELECT e.employee_id, e.first_name, e.last_name, e.department_id, d.department_name
FROM employees e
INNER JOIN departments d  ON(d.department_id = e.department_id);

SELECT e.employee_id, e.first_name, e.last_name,department_id, d.department_name
FROM employees e
INNER JOIN departments d  USING (department_id);

SELECT employee_id, first_name, last_name, department_id, department_name
FROM employees 
NATURAL JOIN departments;

SELECT department_name, city
FROM departments
NATURAL JOIN locations;
