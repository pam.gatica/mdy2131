SELECT sysdate+1
FROM dual;

SELECT last_name, hire_date 
FROM employees
WHERE hire_date = sysdate+1;

SELECT hire_date, sysdate, EXTRACT(MONTH FROM hire_date) , EXTRACT(MONTH FROM sysdate)
FROM employees
WHERE EXTRACT(MONTH FROM hire_date) = EXTRACT(MONTH FROM sysdate) AND EXTRACT(DAY FROM hire_date) = 28;


SELECT 
    sysdate,
    TO_CHAR(sysdate+1,'DD "de" month "de" YYYY'),
    TO_CHAR(sysdate+1,'day'),
    TO_CHAR(sysdate,'HH24')
FROM dual;

SELECT TO_DATE('2020/09/11','YYYY/MM/DD')+1 FROM dual;

SELECT TO_CHAR(sysdate,'CC') FROM dual;

SELECT last_name, salary ,NVL(commission_pct,0), NVL2(commission_pct,CONCAT('0',TO_CHAR(commission_pct)),'SIN COMISION')
FROM employees;