CREATE VIEW v_emp_depto_80 AS
SELECT
    employee_id, last_name, salary
FROM employees
WHERE department_id = 80;

DESC v_emp_depto_80;

SELECT *
FROM v_emp_depto_80
WHERE salary > 10000;


CREATE VIEW v_salarios_depto_50 AS
SELECT
    employee_id id_empleado,
    last_name apellido,
    salary sueldo,
    salary*12 sueldo_anual
FROM employees
WHERE department_id = 50;

SELECT * FROM v_salarios_depto_50;

CREATE VIEW v_dept_sum 
(nombre_depto, minsal, maxsal, avgsal)
AS
SELECT
 d.department_name,
 MIN(e.salary),
 MAX(e.salary), 
 ROUND(AVG(e.salary))
FROM employees e
JOIN departments d ON e.department_id = d.department_id 
GROUP BY d.department_name, d.department_id
ORDER BY d.department_id;

CREATE OR REPLACE VIEW v_emp_10
(employee_id, last_name, job_id) AS
SELECT
employee_id, last_name, job_id
FROM employees
WHERE department_id = 10
WITH READ ONLY;

SELECT * FROM v_emp_10;

DELETE FROM v_emp_10;

DROP VIEW v_emp_10;

CREATE SEQUENCE seq_dept_deptid
INCREMENT BY 10
START WITH 280
MAXVALUE 9999
;

SELECT seq_dept_deptid.currval FROM dual;

INSERT INTO departments (department_id, department_name, location_id)
VALUES (seq_dept_deptid.nextval, 'Support',2500);

CREATE SEQUENCE seq_nuevos_deptos START WITH 1000 INCREMENT BY -1 MAXVALUE 1000;

SELECT
    seq_nuevos_deptos.nextval id_depto,
    department_name,
    location_id
FROM departments
ORDER BY department_name;

CREATE TABLE nuevos_deptos_paso AS
SELECT
    department_name,
    location_id
FROM departments
ORDER BY department_name;

CREATE TABLE nuevos_deptos AS
SELECT
    seq_nuevos_deptos.nextval id_depto,
    department_name,
    location_id
FROM nuevos_deptos_paso;

CREATE OR REPLACE SYNONYM syn_deptos
FOR departments;

SELECT * FROM departments;
SELECT * FROM syn_deptos;
