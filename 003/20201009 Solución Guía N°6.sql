--1.Mostrar nombre, apellido y correo electr�nico de todos los compa�eros de trabajo del empleado de apellido Russell. 
--(Los compa�eros de trabajo son aquellos que comparten el mismo cargo).

--SUBQUERY: Cargo del empleado de apellido Russell
SELECT job_id FROM employees WHERE last_name = 'Russell';

--CONSULTA PRINCIPAL
SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = 'SA_MAN'
AND last_name <> 'Russell';

SELECT
    first_name,
    last_name,
    email
FROM employees
WHERE job_id = (SELECT job_id FROM employees WHERE last_name = 'Russell')
AND last_name <> 'Russell';


--2.Mostrar nombre, apellido, sueldo, cargo de los empleados 
--con sueldo superior al promedio de los sueldos.

--SUBQUERY: Promedio de los sueldos de los empleados
SELECT AVG(salary) FROM employees;

--CONSULTA PRINCIPAL:
SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees
NATURAL JOIN jobs
WHERE salary > 6574
ORDER BY salary;

SELECT
    first_name,
    last_name,
    salary,
    job_title
FROM employees
NATURAL JOIN jobs
WHERE salary > (SELECT AVG(salary) FROM employees)
ORDER BY salary;

--3.	Mostrar id, nombre, apellido, email, tel�fono,
--cargo, departamento, direcci�n, ciudad y pa�s de los 
--empleados con la mayor cantidad de a�os trabajados.

--SUBQUERY: Mayor cantidad de a�os trabajados
SELECT MAX(MONTHS_BETWEEN(SYSDATE,hire_date)/12) FROM employees;

SELECT
    e.employee_id,
    e.first_name,
    e.last_name,
    e.email,
    e.phone_number,
    job_title,
    d.department_name,
    l.street_address,
    l.city,
    c.country_name
FROM employees e
NATURAL JOIN jobs
JOIN departments d USING(department_id)
JOIN locations l USING(location_id)
JOIN countries c USING(country_id)
WHERE MONTHS_BETWEEN(SYSDATE,e.hire_date)/12 = (SELECT MAX(MONTHS_BETWEEN(SYSDATE,hire_date)/12) FROM employees);

--4.Mostrar nombre del departamento y su sueldo promedio de todos los departamentos donde trabajen m�s del promedio de empleados. 

--SUBQUERY: Promedio de empleados por departamento
SELECT AVG(COUNT(employee_id))FROM employees GROUP BY department_id;

--CONSULTA PRINCIPAL:
SELECT
    d.department_name "DEPARTAMENTO",
    TO_CHAR(ROUND(AVG(salary)),'$999G999') "SUELDO PROMEDIO",
    COUNT(e.employee_id)
FROM employees e
JOIN departments d
   USING (department_id)
GROUP BY d.department_name
HAVING COUNT(e.employee_id)> (SELECT AVG(COUNT(employee_id))FROM employees GROUP BY department_id);

--5.Mostrar la cantidad de departamentos por pa�s cuyo sueldo promedio es mayor al promedio de los sueldos.

--SUBQUERY: sueldo promedio de los empleados
SELECT AVG(salary) FROM employees;

--CONSULTA PRINCIPAL:
SELECT 
    c.country_name,
    COUNT(DISTINCT d.department_name)
FROM departments d
JOIN employees e ON(e.department_id = d.department_id)
JOIN locations l USING (location_id)
JOIN countries c USING (country_id)
GROUP BY c.country_name
HAVING AVG(e.salary) > (SELECT AVG(salary) FROM employees);
    