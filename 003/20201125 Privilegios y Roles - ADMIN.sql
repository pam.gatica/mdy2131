--DBA

GRANT CREATE TABLE TO desarrollo_personal;

GRANT SELECT
ON hrcloud.employees
TO desarrollo_personal;

CREATE PUBLIC SYNONYM empleados
FOR hrcloud.employees;

CREATE ROLE rol_gerencia;

GRANT CREATE TABLE, CREATE VIEW TO rol_gerencia;
GRANT SELECT, INSERT, UPDATE, DELETE ON hrcloud.employees TO rol_gerencia;

GRANT rol_gerencia TO desarrollo_personal;
GRANT rol_gerencia TO administrador;
GRANT rol_gerencia TO desarrollo_finanzas;

REVOKE CREATE TABLE FROM desarrollo_personal;

REVOKE rol_gerencia FROM desarrollo_personal;

REVOKE CREATE VIEW FROM rol_gerencia;


