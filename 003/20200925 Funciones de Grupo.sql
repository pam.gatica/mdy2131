SELECT
    salary,
    job_id
FROM employees
WHERE job_id = 'AD_VP';

SELECT
    MAX(salary) "Sueldo Mayor",
    MIN(salary) "Sueldo Menor",
    ROUND(AVG(salary)) "Sueldo Promedio",
    SUM(salary) "Total Sueldos"
FROM employees;


SELECT
    COUNT(*) "Total Empleados",
    COUNT(commission_pct) "Empleados a comision",
    COUNT(DISTINCT department_id) "Departamentos"
FROM employees;

SELECT
    department_id,
    ROUND(AVG(salary)) "Sueldo Promedio",
    MAX(salary) "Sueldo Maximo",
    MIN(salary) "Sueldo Minimo",
    COUNT(employee_id) "Empleados del departamento",
    COUNT(commission_pct)"Empleados a comision"
FROM employees
GROUP BY department_id
ORDER BY department_id;

SELECT
    'En el departamento ' ||
    NVL(TO_CHAR(department_id),'NULO') ||
    ' trabajan ' ||
    COUNT(employee_id) ||
    ' empleados'
FROM employees
GROUP BY department_id
ORDER BY department_id;


SELECT
    department_id,
    job_id,
    SUM(salary)
FROM employees
GROUP BY department_id,job_id
ORDER BY department_id, job_id;

SELECT
    department_id,
    MAX(salary)
FROM employees
GROUP BY department_id
HAVING MAX(salary) > 5000
ORDER BY department_id;
