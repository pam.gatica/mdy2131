--GU�A DE EJERCICIOS N�7
--operadores set

/*
1.Mostrar un listado de nombre del departamento, direcci�n 
y ciudad de los departamentos que se encuentran en am�rica
y en Europa.
*/

SELECT 
    d.department_name,
    street_address,
    city
FROM departments d
NATURAL JOIN locations
NATURAL JOIN countries
NATURAL JOIN regions
WHERE region_name = 'Americas'
UNION
SELECT 
    d.department_name,
    street_address,
    city
FROM departments d
NATURAL JOIN locations
NATURAL JOIN countries
NATURAL JOIN regions
WHERE region_name = 'Europe'
ORDER BY 3;

/*
2.Mostrar id, nombre completo, nombre del cargo, 
sueldo, fecha de t�rmino en el cargo de todos los empleados 
incluyendo su historial de cargos pasados. 
Para el cargo actual utilizar la fecha de hoy como fecha de 
t�rmino, para los trabajos pasados utilizar 0 como valor del sueldo. Ordenar por id del empleado y por fecha en el cargo cronol�gicamente.
*/
SELECT 
    e.employee_id, 
    e.first_name || ' '  || e.last_name "NOMBRE_COMPLETO",
    j.job_title,
    salary,
    sysdate
FROM employees e
JOIN jobs j USING (job_id)
UNION
SELECT
    employee_id,
    e.first_name || ' '  || e.last_name,
    j.job_title,
    0,
    jh.end_date
FROM job_history jh
JOIN employees e USING(employee_id)
JOIN jobs j ON (j.job_id = jh.job_id)
ORDER BY 1,5;

/*
3.Mostrar un listado (Nombre, email y antig�edad en el cargo) 
de todos los empleados que trabajan o han trabajado en el cargo AC_ACCOUNT. Indicar si es su cargo actual o un cargo pasado.
*/

SELECT
    e.first_name || ' ' || e.last_name,
    e.email,
    ROUND(MONTHS_BETWEEN(SYSDATE,e.hire_date)/12) "ANTIGUEDAD",
    'CARGO ACTUAL'
FROM employees e
WHERE job_id = 'AC_ACCOUNT'
UNION
SELECT
    e.first_name || ' '  || e.last_name,
    e.email,
    ROUND(MONTHS_BETWEEN(jh.end_date, jh.start_date)/12),
    'CARGO PASADO'
FROM job_history jh
JOIN employees e USING(employee_id)
WHERE jh.job_id = 'AC_ACCOUNT';

 /*
 4.Mostrar nombre, apellido, sueldo de los empleados 
 que trabajan en los departamentos SA_REP y SA_MAN pero 
 no ganan m�s de 8000 d�lares. 
 (PROPONGA DOS SOLUCIONES PARA ESTE PROBLEMA, UNA DE ELLAS UTILIZANDO OPERADORES SET)
 */
 -- SOLUCI�N USANDO OPERADORES SET
 SELECT first_name, last_name, salary FROM employees
 WHERE job_id = 'SA_REP'
 UNION
 SELECT first_name, last_name, salary FROM employees
 WHERE job_id = 'SA_MAN'
 MINUS
 SELECT first_name, last_name, salary FROM employees
 WHERE salary > 8000
 ORDER BY salary;
 
 --SOLUCI�N SIN OPERADORES SET
  SELECT first_name, last_name, salary FROM employees
  WHERE (job_id = 'SA_MAN' OR job_id = 'SA_REP') AND salary <= 8000
  ORDER BY salary;