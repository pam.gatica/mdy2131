SELECT salary FROM employees WHERE last_name = 'Abel';

SELECT
    last_name,
    salary
FROM employees
WHERE salary > 11000;

SELECT
    last_name,
    salary
FROM employees
WHERE salary > ( SELECT salary FROM employees WHERE last_name = 'Abel' );

SELECT MAX(salary) FROM employees;

SELECT 
    last_name,
    salary
FROM employees
WHERE salary = 30000;

SELECT 
    last_name,
    salary
FROM employees
WHERE salary = (SELECT MAX(salary) FROM employees);

SELECT job_id FROM employees WHERE employee_id = 141;

SELECT 
    last_name,
    salary
FROM employees
WHERE job_id = 'ST_CLERK';

SELECT 
    last_name,
    salary
FROM employees
WHERE job_id = (SELECT job_id FROM employees WHERE employee_id = 141);

SELECT salary FROM employees WHERE employee_id = 143;

SELECT 
    last_name,
    salary
FROM employees
WHERE job_id =  (SELECT job_id FROM employees WHERE employee_id = 141)
AND salary >    (SELECT salary FROM employees WHERE employee_id = 143);


SELECT MIN(salary) FROM employees WHERE department_id = 80;

SELECT
    department_id,
    MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) > 6100;

SELECT
    department_id,
    MIN(salary)
FROM employees
GROUP BY department_id
HAVING MIN(salary) > (SELECT MIN(salary) FROM employees WHERE department_id = 80);


SELECT MIN(salary) FROM employees GROUP BY department_id;

SELECT
    employee_id,
    last_name,
    salary 
FROM employees
WHERE salary IN (2100,6500,8300,17000,2500,10000,7000,4400,6000,4200,6900,6100);

SELECT
    employee_id,
    last_name,
    salary 
FROM employees
WHERE salary IN (SELECT MIN(salary) FROM employees GROUP BY department_id);


SELECT salary FROM employees WHERE department_id = 30;

SELECT
    last_name,
    salary
FROM employees
WHERE salary > ALL(SELECT salary FROM employees WHERE department_id = 30); 


SELECT
    last_name,
    salary
FROM employees
WHERE salary > (SELECT MAX(salary) FROM employees WHERE department_id = 30); 

SELECT DISTINCT salary FROM employees WHERE job_id = 'IT_PROG';

SELECT 
    last_name,
    salary
FROM employees
WHERE salary < ANY(SELECT DISTINCT salary FROM employees WHERE job_id = 'IT_PROG');

CREATE TABLE jefe_empleados AS
SELECT
    emp.employee_id "ID_EMPLEADO",
    emp.first_name || ' ' || emp.last_name "NOMBRE_EMPLEADO",
    CASE
        WHEN emp.manager_id IS NULL THEN 'Sin Jefe'
        WHEN emp.manager_id IS NOT NULL THEN jefe.first_name || ' ' || jefe.last_name
    END
    "NOMBRE_JEFE"
FROM employees emp
LEFT JOIN employees jefe ON (jefe.employee_id = emp.manager_id)
ORDER BY emp.employee_id;