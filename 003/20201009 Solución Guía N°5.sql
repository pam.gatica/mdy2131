--1.Mostrar nombre, apellido, correo electr�nico, tel�fono 
--y cargo de todos los empleados. Ordenar por cargo.
SELECT
    first_name,
    last_name,
    email,
    phone_number,
    job_title
FROM employees 
NATURAL JOIN jobs
ORDER BY job_id;

--2.Mostrar nombre del departamento y 
--nombre y apellido del jefe de todos los departamentos.

SELECT
    d.department_name,
    e.first_name|| ' ' || e.last_name "JEFE"
FROM departments d
LEFT JOIN employees e ON (e.employee_id = d.manager_id);

--3.Mostrar nombre del departamento, direcci�n,
--c�digo postal, ciudad y provincia de todos los departamentos.

SELECT
    d.department_name,
    l.street_address,
    l.postal_code,
    l.city,
    l.state_province
FROM departments d
JOIN locations l USING (location_id);

--4.Mostrar cargo, fecha de inicio del cargo 
--y fecha de t�rmino del cargo para un empleado. 
--Use variables de sustituci�n para ingresar el id de un empleado.


SELECT
    e.employee_id,
    e.first_name,
    e.last_name,
    js.job_id,
    js.start_date,
    js.end_date
FROM employees e
JOIN job_history js ON (js.employee_id = e.employee_id)
WHERE e.employee_id = &codigo_empleado;

--5.Mostrar la cantidad de departamentos ubicados en cada ciudad. 
--Mostrar el nombre de cada ciudad.

SELECT
    l.city,
    COUNT(department_id)
FROM departments d
JOIN locations l USING(location_id)
GROUP BY l.city;