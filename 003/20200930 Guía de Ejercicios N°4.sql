-- GU�A DE EJERCICIOS N�4
-- FUNCIONES DE GRUPO

--1.Mostrar el promedio de sueldos de todos los departamentos.

SELECT
    department_id DEPARTAMENTO,
    TO_CHAR(ROUND(AVG(salary)),'$999G999') "SUELDO PROMEDIO"
FROM employees
GROUP BY department_id
ORDER BY AVG(salary);

--2.Mostrar la cantidad de departamentos ubicados en cada sucursal. (Locations)
SELECT
    location_id SUCURSAL,
    COUNT(department_id) DEPARTAMENTOS
FROM departments
GROUP BY location_id
ORDER BY location_id;

--3.Mostrar la antig�edad m�xima, m�nima y el promedio de 
--antig�edad de todos los empleados.
SELECT
    MAX(ROUND(MONTHS_BETWEEN(SYSDATE,hire_date)/12)) "ANTIGUEDAD MAYOR",
    MIN(ROUND(MONTHS_BETWEEN(SYSDATE,hire_date)/12)) "ANTIGUEDAD MENOR",
    ROUND(AVG(MONTHS_BETWEEN(SYSDATE,hire_date)/12)) "ANTIGUEDAD PROMEDIO"
FROM employees;

--4.Mostrar el promedio de antig�edad de cada cargo.
SELECT
    job_id "CARGO",
    ROUND(AVG(MONTHS_BETWEEN(SYSDATE,hire_date)/12)) "ANTIGUEDAD PROMEDIO"
FROM employees
GROUP BY job_id
ORDER BY job_id;

--5.Mostrar la ID y total de gente a cargo de todos los empleados que tienen jefatura.
SELECT
    manager_id JEFE,
    COUNT(employee_id) "EMPLEADOS A CARGO"
FROM employees
GROUP BY manager_id
ORDER BY manager_id;

--6.Mostrar la fecha de contrataci�n mas reciente de cada departamento.
SELECT
    department_id,
    MAX(hire_date)
FROM employees
GROUP BY department_id
ORDER BY department_id;

--7.Mostrar el sueldo promedio de todos los cargos de jefatura. (_MGR o _MAN)
SELECT
    job_id "CARGO",
    TO_CHAR(AVG(salary),'$999G999') "SUELDO PROMEDIO"
FROM employees
WHERE job_id LIKE '%_MAN' OR job_id LIKE '%_MGR'
GROUP BY job_id;

--8.Modificar la sentencia anterior para mostrar s�lo los departamentos cuyo sueldo promedio sea superior a USD 10000.
SELECT
    job_id "CARGO",
    TO_CHAR(AVG(salary),'$999G999') "SUELDO PROMEDIO"
FROM employees
WHERE job_id LIKE '%_MAN' OR job_id LIKE '%_MGR'
GROUP BY job_id
HAVING AVG(salary) > 10000;