INSERT ALL
    INTO puntos_cliente(periodo, run, puntaje_periodo)
    VALUES(TO_CHAR(sysdate,'YYYY'),run,puntaje)
SELECT
    run,
    NVL(SUM(puntaje),0) puntaje
FROM puntos_boleta
JOIN boleta USING (numero_boleta)
JOIN pedido USING (cod_pedido)
JOIN carro USING (cod_carro)
RIGHT JOIN cliente USING (run)
GROUP BY run
ORDER BY run;

INSERT ALL
    INTO categoria_cliente(periodo, run, categoria, puntos, descuento)
    VALUES(periodo,run,categoria,puntaje_periodo,descuento)
SELECT
    EXTRACT(YEAR FROM sysdate)+1 periodo,
    run,
    categoria,
    puntaje_periodo,
    descuento
FROM puntos_cliente
JOIN categoria ON puntaje_periodo BETWEEN puntaje_desde AND puntaje_hasta
ORDER BY run;

SELECT
cod_carro 
FROM carro
WHERE EXTRACT(MONTH FROM fecha_creacion) = EXTRACT(MONTH FROM sysdate) -1 
MINUS
SELECT
    cod_carro
FROM pedido
WHERE EXTRACT(MONTH FROM fecha_pedido) = EXTRACT(MONTH FROM sysdate) -1 ;


INSERT ALL
    INTO promo_carritos(run,fecha_inicio_promo, fecha_fin_promo, descuento)
    VALUES (run, fecha_ini, fecha_fin, descuento)
SELECT
    run,
    TRUNC(sysdate,'MM') fecha_ini,
    LAST_DAY(sysdate) fecha_fin,
    descuento+15 descuento
FROM carro
JOIN cliente USING(run)
JOIN categoria_cliente USING (run)
WHERE cod_carro IN (    SELECT cod_carro FROM carro
                        WHERE EXTRACT(MONTH FROM fecha_creacion) = EXTRACT(MONTH FROM sysdate) -1 
                        MINUS
                        SELECT cod_carro FROM pedido
                        WHERE EXTRACT(MONTH FROM fecha_pedido) = EXTRACT(MONTH FROM sysdate) -1 );
                
INSERT ALL
    INTO promo_carritos(run,fecha_inicio_promo, fecha_fin_promo, descuento)
    VALUES (run, fecha_ini, fecha_fin, descuento)
SELECT
run,
TRUNC(sysdate,'MM') fecha_ini,
LAST_DAY(sysdate) fecha_fin,
descuento + 15 descuento
FROM carro
LEFT JOIN pedido USING(cod_carro)
JOIN categoria_cliente USING(run)
WHERE EXTRACT(MONTH FROM fecha_creacion) = EXTRACT(MONTH FROM sysdate) -1
AND cod_pedido IS NULL;

INSERT INTO pedido (cod_pedido, fecha_pedido,direccion_envio, cod_carro)
VALUES(
    (SELECT MAX(cod_pedido)+1 FROM pedido),
    sysdate,
    'Alvarez 2336, Vi�a del Mar',
(SELECT cod_carro FROM carro WHERE fecha_creacion = '07/10/2020'
AND run = (SELECT run FROM cliente WHERE nombre = 'Kalindi' AND apellido= 'Pettisall'))
);

INSERT INTO estado (cod_estado, cod_pedido, fecha_estado)
VALUES(
    'EP',
    (SELECT MAX(cod_pedido) FROM pedido),
    sysdate
);

UPDATE producto
SET precio = ROUND(precio - (precio * 10 /100))
WHERE cod_producto = 
(SELECT cod_producto 
    FROM pedido
    JOIN carro USING(cod_carro)
    JOIN producto_carro USING (cod_carro)
    GROUP BY cod_producto
    HAVING SUM(cantidad) = (SELECT MAX(SUM(cantidad))FROM pedido
                            JOIN carro USING(cod_carro)
                            JOIN producto_carro USING (cod_carro)
                            GROUP BY cod_producto)
);

