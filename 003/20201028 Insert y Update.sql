INSERT INTO departments (department_id, department_name, manager_id, location_id)
VALUES (400,'Ventas',100,1700);

INSERT INTO departments(manager_id, location_id, department_name, department_id)
VALUES (101,1700,'Compras',500);

INSERT INTO departments
VALUES (600,'Bienestar',100,1700);

INSERT INTO departments (department_id, department_name)
VALUES (700,'Capacitación');

INSERT INTO departments
VALUES (800,'Recreación',NULL,NULL);

SAVEPOINT A;

INSERT INTO employees
VALUES(300,'Peter','Parker','PPARKER','569.9999.3333',SYSDATE,'AC_ACCOUNT',10000,NULL,100,90);

ROLLBACK TO A;

SELECT MAX(employee_id)+1  FROM employees;


INSERT INTO employees
VALUES((SELECT MAX(employee_id)+1  FROM employees),'Peter','Parker','PPARKER','569.9999.3333',SYSDATE,'AC_ACCOUNT',10000,NULL,100,90);


SELECT department_name
FROM departments
WHERE location_id = 1700;

INSERT ALL
    INTO copia_depto(nombre)
    VALUES(department_name)
SELECT department_name
FROM departments
WHERE location_id = 1700;


CREATE TABLE depto_con_jefe
( ID NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 9999999999 INCREMENT BY 1 START WITH 1,
NOMBRE_DEPTO VARCHAR(30) NOT NULL,
MANAGER_ID NUMBER(6)
);

CREATE TABLE depto_sin_jefe
( ID NUMBER(10) GENERATED ALWAYS AS IDENTITY MINVALUE 10 MAXVALUE 9999999999 INCREMENT BY 10 START WITH 10,
NOMBRE_DEPTO VARCHAR(30) NOT NULL
);

SELECT department_name, manager_id
FROM departments;

INSERT ALL
    WHEN manager_id IS NULL THEN
        INTO depto_sin_jefe (nombre_depto) VALUES (department_name)
    ELSE
        INTO depto_con_jefe(nombre_depto, manager_id) VALUES(department_name, manager_id)
SELECT department_name, manager_id
FROM departments;

INSERT INTO employees
VALUES((SELECT MAX(employee_id)+1  FROM employees),'Diana','Austin','DIAUSTIN','569.9999.3333',SYSDATE,'AC_ACCOUNT',10000,NULL,100,90);


SAVEPOINT B;

UPDATE employees SET department_id = 110 WHERE employee_id = 103;
UPDATE employees SET department_id = 110 WHERE employee_id = 104;

ROLLBACK TO B;

UPDATE employees SET department_id = 110 WHERE employee_id = 103 OR employee_id = 104;

ROLLBACK TO B;
UPDATE employees SET department_id = 110 WHERE employee_id IN (103,104);

SAVEPOINT C;
UPDATE employees SET salary = salary*1.5, commission_pct = 0.01 WHERE employee_id = 101;
ROLLBACK TO C;

SAVEPOINT D;
UPDATE employees
SET salary = 
    CASE
        WHEN EXTRACT(YEAR FROM hire_date) = 2004 THEN ROUND(salary*1.15)
        WHEN EXTRACT(YEAR FROM hire_date) = 2005 THEN ROUND(salary*1.13)
        ELSE ROUND(salary*1.105)
    END;
